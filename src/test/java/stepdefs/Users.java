package stepdefs;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojo.Post;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

public class Users {
    private final World world;
    private String uri;
    private RequestSpecification request;
    private Response response;

    public Users(World world) {
        this.world = world;
    }

    @Given("^I have a service to request list of users$")
    public void iHaveAServiceToRequestListOfUsers() {
        request = given();
        		
        uri = String.format("%s/%s", world.url, "users");
       
        get(uri).then().assertThat().statusCode(200);

    }
    
    @When("^I request list of users$")
    public void iRequestListOfUsers() {
        response = request
                .get(uri);
        world.response = response;
    }
}
