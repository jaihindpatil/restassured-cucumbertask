Feature: Get list of of users
  
  @test
  Scenario: Call users end point and check response code
    Given I have a service to request list of users
    When I request list of users
    Then the status code is 200