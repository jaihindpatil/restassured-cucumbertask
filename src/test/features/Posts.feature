Feature: Send new post

  @test
  Scenario: Make new posts to server and check response code
    Given I have an end point posts
    When I post with title "My API test title" and author "my name"
    Then the status code is 201